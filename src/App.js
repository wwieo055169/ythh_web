import './style/App.css'
import React, { Component } from 'react';
import {Switch , Route} from 'react-router-dom';

import NavBar from './components/NavBar';

import Home from './pages/Home'
import Activity from './pages/Activity'
import Crew from './pages/Crew'
import History from './pages/History'
import Introduction from './pages/Introduction'
import Purpose from './pages/Purpose'
import Songs from './pages/Songs'

class App extends Component {
  render() {
    return (
      <div className='container'>
        <NavBar />
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/activity" component={Activity} />
          <Route exact path="/crew" component={Crew} />
          <Route exact path="/history" component={History} />
          <Route exact path="/introduction" component={Introduction} />
          <Route exact path="/purpose" component={Purpose} />
          <Route exact path="/songs" component={Songs} />
        </Switch>
      </div>
    );
  }
}

export default App;
