import React from 'react';
import JumpBotron from './../components/JumpBotron'


class Home extends React.Component{
  render(){
    return (
      <div>
        <JumpBotron/>
        <h2>Hello, world!</h2>
      </div>
    );
  };
};
export default Home;
